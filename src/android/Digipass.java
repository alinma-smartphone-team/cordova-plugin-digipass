package cordova.plugin.digipass;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.LOG;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.telecom.Call;
import android.util.Log;

import com.vasco.digipass.sdk.DigipassSDK;
import com.vasco.digipass.sdk.DigipassSDKConstants;
import com.vasco.digipass.sdk.DigipassSDKReturnCodes;
import com.vasco.digipass.sdk.responses.ActivationResponse;
import com.vasco.digipass.sdk.responses.DigipassPropertiesResponse;
import com.vasco.digipass.sdk.responses.GenerationResponse;
import com.vasco.digipass.sdk.responses.GenericResponse;
import com.vasco.digipass.sdk.responses.ValidationResponse;
import com.vasco.digipass.sdk.utils.devicebinding.DeviceBindingSDKErrorCodes;
import com.vasco.digipass.sdk.utils.securestorage.SecureStorageSDK;
import com.vasco.digipass.sdk.utils.securestorage.SecureStorageSDKErrorCodes;
import com.vasco.digipass.sdk.utils.securestorage.SecureStorageSDKException;

import com.vasco.digipass.sdk.utils.devicebinding.DeviceBindingSDK;
import com.vasco.digipass.sdk.utils.devicebinding.DeviceBindingSDKException;
import com.vasco.digipass.sdk.utils.devicebinding.DeviceBindingSDKParams;
import com.vasco.digipass.sdk.utils.utilities.UtilitiesSDK;

/**
 * This class echoes a string called from JavaScript.
 */
public class Digipass extends CordovaPlugin {

    public static final String TAG = "DIGIPASS";
    public static final String DEVICE_BINDING_TAG = "DIGIPASS DEVICE BINDING";
    public static final String STORAGE_TAG = "DIGIPASS SECURE STORAGE";
    public static final String PLUGIN_TAG = "DIGIPASS PLUGIN";

    public static final String ACTIVATED = "ACTIVATED";
    public static final String NOT_REGISTERED = "NOT_REGISTERED";
    public static final String NOT_ACTIVATED = "NOT_ACTIVATED";
    public static final String UNKNOWN_ERROR = "UNKNOWN_ERROR";
    public static final String CORRECT_PASSWORD = "CORRECT_PASSWORD";
    public static final String PASSWORD_CHANGED = "PASSWORD_CHANGED";
    public static final String TOKEN_ACTIVATED = "TOKEN ACTIVATED SUCCESSFULLY";

    public static final String SS_SV_KEY = "245341gdf231"; // recommended to be changed to code
    public static final String SS_DV_KEY = "234jfwno343s";

    private SecureStorageSDK secureStorage;

    /**
     * A salt value which can be included to the secure storage fingerprint computation.
     * Must be different for each platform (Android, iOS, Windows Phone...) and not trivial (e.g. a long random value).
     */
    private static final String SECURE_STORAGE_SALT = "3c23f88dfbf7057e6baa6105b4aa5c5fb3a920ed729c0ba6738a1b44844e95f6";

    /**
     * A salt value which can be included to the device fingerprint computation.
     * Must be different for each platform (Android, iOS, Windows Phone...) and not trivial (e.g. a long random value).
     */
    private static final String DIGIPASS_SALT = "f8eaa859da36cc399327cc3cb55a591d772ece0c5de62b24bdc729eca840ff28";

    /**
     * A static vector value which unique for Alinma tokens.
     */
    private static final String STATIC_VECTOR_DEFAULT = "380800E6010346444C02109D4F3D91CEEA26AD2CBC6ED38C5D611A0301010401040501040601010701010801050901040A01000B01010C01000E01000F0101100101380103112F120100130101140101150C4F545020202020202020202016010117044080F0021801002901062A01002B01002C01021135120100130101140102150C43522020202020202020202016010117044183F4D51801011901062101062901062A01002B01002C0102113B120100130101140103150C5349474E202020202020202016010117044183F4D51801021901041A01042101102201102901062A01002B01002C0102"; //STATIC VALUE.

    private byte[] dynamicVectorResponse, staticVectorResponse;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("initSecureStorage")) {
            String fileName = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    initSecureStorage(fileName, callbackContext);
                }
            });
            return true;
        } else if (action.equals("putStringInSecureStorage")) {
            String code = args.getString(0);
            String value = args.getString(1);
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    putStringInSecureStorage(code, value, callbackContext);
                }
            });
            return true;
        } else if (action.equals("getStringFromSecureStorage")) {
            String code = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    getStringFromSecureStorage(code, callbackContext);
                }
            });
            return true;
        } else if (action.equals("putByteInSecureStorage")) {
            String code = args.getString(0);
            byte[] value = args.getString(1).getBytes();
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    putByteInSecureStorage(code, value, callbackContext);
                }
            });
            return true;
        } else if (action.equals("getByteFromSecureStorage")) {
            String code = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    getByteFromSecureStorage(code, callbackContext);
                }
            });
            return true;
        } else if (action.equals("getDigipassStatus")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    getDigipassStatus(callbackContext);
                }
            });
            return true;
        } else if (action.equals("getDigipassProperties")) {
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    getDigipassProperties(callbackContext);
                }
            });
            return true;
        } else if (action.equals("activateToken")) {
            String serialNumber = args.getString(0);
            String activationCode = args.getString(1);
            String eventReactivationCounter = args.getString(2);
            String password = args.getString(3);
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    activateToken(serialNumber, activationCode, eventReactivationCounter, password, callbackContext);
                }
            });
            return true;
        } else if (action.equals("generateOTP")) {
            String password = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    generateOTP(password, callbackContext);
                }
            });
            return true;
        } else if (action.equals("validatePassword")) {
            String password = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    validatePassword(password, callbackContext);
                }
            });
            return true;
        } else if (action.equals("changePassword")) {
            String oldPassword = args.getString(0);
            String newPassword = args.getString(1);
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    changePassword(oldPassword, newPassword, callbackContext);
                }
            });
            return true;
        } else if (action.equals("isPasswordWeak")) {
            String password = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    isPasswordWeak(password, callbackContext);
                }
            });
            return true;
        } else if (action.equals("generateSignature")) {
            String password = args.getString(0);
            JSONArray dataFieldsJSON = args.getJSONArray(1);
            String[] dataFields = new String[dataFieldsJSON.length()];
            for (int i = 0; i < dataFieldsJSON.length(); i++) {
                dataFields[i] = dataFieldsJSON.getString(i);
            }
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    generateSignature(dataFields, password, callbackContext);
                }
            });
            return true;
        } else if (action.equals("computeClientServerTimeShiftFromServerTime")) {
            long serverTimeInSeconds = args.getLong(0);
            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    computeClientServerTimeShiftFromServerTime(serverTimeInSeconds, callbackContext);
                }
            });
            return true;
        } else if (action.equals("deactivateToken")) {

            cordova.getThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                    deactivateToken(callbackContext);
                }
            });
            return true;
        }


        return false;
    }

    private void initSecureStorage(String fileName, CallbackContext callbackContext) {
        if (fileName != null && fileName.length() > 0) {
            // Initialize a secure storage
            try {
                secureStorage = SecureStorageSDK.init(fileName, getSecureStorageFingerprint(), getIterationNumber(), this.cordova.getActivity().getApplicationContext());
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, STORAGE_TAG + "Storage created successfully and file name is: " + secureStorage.toString()));
            } catch (SecureStorageSDKException e) {
                Log.e(STORAGE_TAG, "ERROR ON SECURE STORAGE INIT, " + getSecureStorageErrorMessage(e));
                callbackContext.error(buildPluginErrorResultObject(e.getErrorCode(), "ERROR ON SECURE STORAGE INIT, " + getSecureStorageErrorMessage(e)));
            }

        } else {
            Log.e(STORAGE_TAG, "ERROR ON SECURE STORAGE INIT, " + "no file name provided");
            callbackContext.error("ERROR ON SECURE STORAGE INIT, " + "no file name provided");
        }
    }

    private void putStringInSecureStorage(String key, String value, CallbackContext callbackContext) {
        putStringInSecureStorage(key, value);
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, STORAGE_TAG + ":" + key + " was written successfully"));
    }

    private void putByteInSecureStorage(String key, byte[] value, CallbackContext callbackContext) {
        putByteInSecureStorage(key, value);
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, STORAGE_TAG + ":" + key + " was written successfully"));
    }

    private void getStringFromSecureStorage(String key, CallbackContext callbackContext) {
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getStringFromSecureStorage(key)));
    }

    private void getByteFromSecureStorage(String key, CallbackContext callbackContext) {
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getByteFromSecureStorage(key)));
    }

    private void getDigipassStatus(CallbackContext callbackContext) {
        String status = null;
        try {
            if (!secureStorage.contains(SS_SV_KEY) || !secureStorage.contains(SS_DV_KEY)) {
                status = NOT_REGISTERED;
            }

            DigipassPropertiesResponse dpr = DigipassSDK.getDigipassProperties(getByteFromSecureStorage(SS_SV_KEY), getByteFromSecureStorage(SS_DV_KEY));

            if (dpr.getReturnCode() != DigipassSDKReturnCodes.SUCCESS || dpr.getStatus() != DigipassSDKConstants.STATUS_ACTIVATED) {
                status = NOT_ACTIVATED;
            } else {
                status = ACTIVATED;
            }
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, status));

        } catch (SecureStorageSDKException e) {
            Log.e(STORAGE_TAG, "ERROR ON GET DIGIPASS STATUS, " + getSecureStorageErrorMessage(e));
            callbackContext.error(buildPluginErrorResultObject(e.getErrorCode(), "ERROR ON GET DIGIPASS STATUS, " + getSecureStorageErrorMessage(e)));
        }

    }

    private void getDigipassProperties(CallbackContext callbackContext) {
        JSONObject properties = new JSONObject();

        try {
            if (!secureStorage.contains(SS_SV_KEY)) {
                Log.e(STORAGE_TAG, "ERROR ON GET DIGIPASS PROPERTIES, " + NOT_REGISTERED);
                callbackContext.error(buildPluginErrorResultObject(0, "ERROR ON GET DIGIPASS PROPERTIES, " + NOT_REGISTERED));
            } else {
                DigipassPropertiesResponse dpr = DigipassSDK.getDigipassProperties(getByteFromSecureStorage(SS_SV_KEY), getByteFromSecureStorage(SS_DV_KEY));
                Log.i(PLUGIN_TAG, dpr.getReturnCode() + ": " + DigipassSDK.getMessageForReturnCode(dpr.getReturnCode()));

                if (dpr.getReturnCode() != DigipassSDKReturnCodes.SUCCESS || dpr.getStatus() != DigipassSDKConstants.STATUS_ACTIVATED) {
                    Log.e(PLUGIN_TAG, "ERROR ON GET DIGIPASS PROPERTIES, " + NOT_REGISTERED);
                    callbackContext.error(buildPluginErrorResultObject(dpr.getReturnCode(), "ERROR ON GET DIGIPASS PROPERTIES, " + NOT_ACTIVATED));
                } else {
                    try {
                        properties.put("serialNumber", dpr.getSerialNumber());
                        properties.put("phoneTime", dpr.getUtcTime());
                        properties.put("timeShift", getClientServerTimeShift());
                        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, properties));
                    } catch (JSONException e) {
                        Log.e(STORAGE_TAG, "ERROR ON GET DIGIPASS PROPERTIES, " + e.getMessage());
                        callbackContext.error(buildPluginErrorResultObject(e.hashCode(), "ERROR ON GET DIGIPASS PROPERTIES, " + e.getMessage()));
                    }
                }
            }
        } catch (SecureStorageSDKException e) {
            Log.e(STORAGE_TAG, "ERROR ON GET DIGIPASS PROPERTIES, " + getSecureStorageErrorMessage(e));
            callbackContext.error(buildPluginErrorResultObject(e.getErrorCode(), "ERROR ON GET DIGIPASS PROPERTIES, " + getSecureStorageErrorMessage(e)));
            e.printStackTrace();
        }

    }

    private void activateToken(String serialNumber, String activationCode, String eventReactivationCounter, String password, CallbackContext callbackContext) {

        ActivationResponse activationResponse = DigipassSDK.activateOfflineWithFingerprint(STATIC_VECTOR_DEFAULT, serialNumber, activationCode, eventReactivationCounter, null, password, getDigipassFingerprint(), null);
        dynamicVectorResponse = activationResponse.getDynamicVector(); //update the dynamic vector value
        staticVectorResponse = activationResponse.getStaticVector();

        putByteInSecureStorage(SS_SV_KEY, activationResponse.getStaticVector());
        putByteInSecureStorage(SS_DV_KEY, activationResponse.getDynamicVector());

        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, TOKEN_ACTIVATED));

    }

    private void generateOTP(String password, CallbackContext callbackContext) {

        GenerationResponse generateResponse = DigipassSDK.generateResponseOnly(getByteFromSecureStorage(SS_SV_KEY),
                getByteFromSecureStorage(SS_DV_KEY), password, getClientServerTimeShift(),
                DigipassSDKConstants.CRYPTO_APPLICATION_INDEX_APP_1, getDigipassFingerprint());

        if (generateResponse.getReturnCode() != DigipassSDKReturnCodes.SUCCESS) {
            callbackContext.error(buildPluginErrorResultObject(generateResponse.getReturnCode(), "THE PASSWORD GENERATION HAS FAILED - " + getPasswordRelatedError(generateResponse)));
        } else {
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, generateResponse.getResponse()));
        }

        putByteInSecureStorage(SS_DV_KEY, generateResponse.getDynamicVector());
    }

    private void validatePassword(String password, CallbackContext callbackContext) {

        ValidationResponse validateResponse = DigipassSDK.validatePasswordWithFingerprint(getByteFromSecureStorage(SS_SV_KEY),
                getByteFromSecureStorage(SS_DV_KEY), password, getDigipassFingerprint());

        if (validateResponse.getReturnCode() != DigipassSDKReturnCodes.SUCCESS) {
            callbackContext.error(buildPluginErrorResultObject(validateResponse.getReturnCode(), "THE USER PASSWORD VALIDATION HAS FAILED - " + DigipassSDK.getMessageForReturnCode(validateResponse.getReturnCode())));
        } else {
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, CORRECT_PASSWORD));
        }

        putByteInSecureStorage(SS_DV_KEY, validateResponse.getDynamicVector());
    }

    private void changePassword(String oldPassword, String newPassword, CallbackContext callbackContext) {

        GenericResponse genericResponse = DigipassSDK.changePasswordWithFingerprint(getByteFromSecureStorage(SS_SV_KEY),
                getByteFromSecureStorage(SS_DV_KEY), oldPassword, newPassword, getDigipassFingerprint());

        if (genericResponse.getReturnCode() != DigipassSDKReturnCodes.SUCCESS) {
            callbackContext.error(buildPluginErrorResultObject(genericResponse.getReturnCode(), "THE USER PASSWORD WAS NOT CHANGED - " + DigipassSDK.getMessageForReturnCode(genericResponse.getReturnCode())));
        } else {
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, PASSWORD_CHANGED));
        }

        putByteInSecureStorage(SS_DV_KEY, genericResponse.getDynamicVector());
    }

    private void isPasswordWeak(String password, CallbackContext callbackContext) {
        boolean isPasswordWeak = DigipassSDK.isPasswordWeak(password);
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, isPasswordWeak));

    }

    private void generateSignature(String[] dataFields, String password, CallbackContext callbackContext) {
        GenerationResponse generateResponse = DigipassSDK.generateSignature(getByteFromSecureStorage(SS_SV_KEY),
                getByteFromSecureStorage(SS_DV_KEY), dataFields, password, getClientServerTimeShift(),
                DigipassSDKConstants.CRYPTO_APPLICATION_INDEX_APP_3, getDigipassFingerprint());

        if (generateResponse.getReturnCode() != DigipassSDKReturnCodes.SUCCESS) {
            callbackContext.error(buildPluginErrorResultObject(generateResponse.getReturnCode(), "THE SIGNATURE GENERATION HAS FAILED - " + DigipassSDK.getMessageForReturnCode(generateResponse.getReturnCode())));
        } else {
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, generateResponse.getResponse()));
        }
        putByteInSecureStorage(SS_DV_KEY, generateResponse.getDynamicVector());
    }

    private void deactivateToken(CallbackContext callbackContext) {
        try {
            removeFromSecureStorage(SS_DV_KEY);
            writeSecureStorage();
            callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK));
        } catch (Exception e) {
            Log.e(STORAGE_TAG, "ERROR ON SECURE STORAGE GET. CODE" + e.hashCode() + " MESSAGE " + e.getMessage());
        }

    }

    private void computeClientServerTimeShiftFromServerTime(long serverTimeInSeconds, CallbackContext callbackContext) {
        long clientServerTimeShift = DigipassSDK.computeClientServerTimeShiftFromServerTime(serverTimeInSeconds);
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, clientServerTimeShift));
    }

    private JSONObject buildPluginErrorResultObject(int code, String message) {
        JSONObject pluginErrorResultObject = new JSONObject();
        try {
            pluginErrorResultObject.put("errorCode", code);
            pluginErrorResultObject.put("errorMessage", message);
        } catch (JSONException e) {
            Log.e(PLUGIN_TAG, "ERROR ON BUILDING PLUGIN ERROR RESULT");
        }
        return pluginErrorResultObject;
    }

    private static String getPasswordRelatedError(GenericResponse gr) {
        String error = DigipassSDK.getMessageForReturnCode(gr.getReturnCode());
        if (gr.getReturnCode() == DigipassSDKReturnCodes.PASSWORD_WRONG) {
            error = "INVALID PIN" + "" + gr.getAttemptLeft() + " ATTEMPTS LEFT";
        }

        if (gr.getReturnCode() == DigipassSDKReturnCodes.PASSWORD_LENGTH_TOO_SHORT) {
            error = "PIN IS TOO SHORT" + "" + gr.getAttemptLeft() + " ATTEMPTS LEFT";
        }

        if (gr.getReturnCode() == DigipassSDKReturnCodes.PASSWORD_LENGTH_TOO_LONG) {
            error = "PIN IS TOO LONG" + "" + gr.getAttemptLeft() + " ATTEMPTS LEFT";
        }

        if (gr.getReturnCode() == DigipassSDKReturnCodes.PASSWORD_WEAK) {
            error = "PIN IS WEAK OR THE SAME" + "" + gr.getAttemptLeft() + " ATTEMPTS LEFT";
        }

        if (gr.getReturnCode() == DigipassSDKReturnCodes.PASSWORD_NULL) {
            error = "NO PIN WAS PROVIDED" + "" + gr.getAttemptLeft() + " ATTEMPTS LEFT";
        }

        return error;
    }

    // Get a readable error message from an SecureStorageSDKException
    private String getSecureStorageErrorMessage(SecureStorageSDKException e) {
        switch (e.getErrorCode()) {
            case SecureStorageSDKErrorCodes.INTERNAL_ERROR:
                return "Internal error: " + e.getMessage();
            case SecureStorageSDKErrorCodes.STORAGE_NAME_NULL:
                return "Name of the storage null";
            case SecureStorageSDKErrorCodes.STORAGE_NAME_INCORRECT_LENGTH:
                return "Name of the storage too long";
            case SecureStorageSDKErrorCodes.STORAGE_NAME_INCORRECT_FORMAT:
                return "Name of the storage contains invalid characters. Must be alphanumeric";
            case SecureStorageSDKErrorCodes.UNKNOWN_STORAGE:
                return "Storage does not exist";
            case SecureStorageSDKErrorCodes.UNREADABLE_STORAGE:
                return "Storage not readable";
            case SecureStorageSDKErrorCodes.CONTEXT_NULL:
                return "Android context null";
            case SecureStorageSDKErrorCodes.ITERATION_COUNT_INCORRECT:
                return "Iteration count must be >0";
            case SecureStorageSDKErrorCodes.KEY_NULL:
                return "Key null";
            case SecureStorageSDKErrorCodes.KEY_INCORRECT_LENGTH:
                return "Key has incorrect length";
            case SecureStorageSDKErrorCodes.KEY_INCORRECT_FORMAT:
                return "Key contains invalid character";
            case SecureStorageSDKErrorCodes.UNKNOWN_KEY:
                return "Storage does not contains requested key";
            case SecureStorageSDKErrorCodes.VALUE_NULL:
                return "Value null";
            case SecureStorageSDKErrorCodes.VALUE_INCORRECT_FORMAT:
                return "Value contains invalid character";
            default:
                return "Unknown error";
        }
    }

    // Get a readable error message from an DeviceBindingSDKException
    private String getDeviceBindingErrorMessage(DeviceBindingSDKException e) {
        switch (e.getErrorCode()) {
            case DeviceBindingSDKErrorCodes.CONTEXT_NULL:
                return ("Provided context is null");
            case DeviceBindingSDKErrorCodes.PERMISSION_DENIED:
                return ("Permission denied");
            case DeviceBindingSDKErrorCodes.SALT_NULL:
                return ("Salt is null");
            case DeviceBindingSDKErrorCodes.SALT_TOO_SHORT:
                return ("Salt is too short");
            case DeviceBindingSDKErrorCodes.UNIQUE_DATA_UNDEFINED:
                return ("Unique data is undefined");
            case DeviceBindingSDKErrorCodes.INTERNAL_ERROR:
                return ("Internal error");
            default:
                return "Unknown error";
        }
    }

    private void writeSecureStorage() {
        try {
            secureStorage.write(getSecureStorageFingerprint(), getIterationNumber(), this.cordova.getActivity().getApplicationContext());
        } catch (SecureStorageSDKException e) {
            Log.e(STORAGE_TAG, "ERROR ON SECURE STORAGE WRITE. CODE" + e.getErrorCode() + " MESSAGE " + e.getMessage());
        }
    }

    private void putStringInSecureStorage(String key, String value) {
        try {
            secureStorage.putString(key, value);
            writeSecureStorage();
        } catch (SecureStorageSDKException e) {
            Log.e(STORAGE_TAG, "ERROR ON PUT IN SECURE STORAGE. CODE" + e.getErrorCode() + " MESSAGE " + e.getMessage());
        }
    }

    private void putByteInSecureStorage(String key, byte[] value) {
        try {
            secureStorage.putBytes(key, value);
            writeSecureStorage();
        } catch (SecureStorageSDKException e) {
            Log.e(STORAGE_TAG, "ERROR ON PUT IN SECURE STORAGE. CODE" + e.getErrorCode() + " MESSAGE " + e.getMessage());
        }
    }

    private String getStringFromSecureStorage(String key) {
        String value = null;
        try {
            value = secureStorage.getString(key);
        } catch (SecureStorageSDKException e) {
            Log.e(STORAGE_TAG, "ERROR ON SECURE STORAGE GET. CODE" + e.getErrorCode() + " MESSAGE " + e.getMessage());
        }
        return value;
    }

    private byte[] getByteFromSecureStorage(String key) {
        try {
            if (secureStorage.contains(key)) {
                return secureStorage.getBytes(key);
            } else {
                return null;
            }
        } catch (SecureStorageSDKException e) {
            Log.e(STORAGE_TAG, "ERROR ON SECURE STORAGE GET. CODE" + e.getErrorCode() + " MESSAGE " + e.getMessage());
        }
        return null;
    }

    private void removeFromSecureStorage(String key) {
        try {
            secureStorage.remove(key);
        } catch (SecureStorageSDKException e) {
            Log.e(STORAGE_TAG, "ERROR ON SECURE STORAGE GET. CODE" + e.getErrorCode() + " MESSAGE " + e.getMessage());
        }
    }

    /**
     * Return the recommended iteration number to support old Android devices.
     * If you don't need the support of old devices the recommended iteration number is 8000.
     *
     * @return The recommended iteration number to support old Android devices.
     */
    private int getIterationNumber() {
        return 300;
    }

    /**
     * Return the storage fingerprint.
     * The storage fingerprint must be a unique identifier generated by the DeviceBindingSDK
     * DeviceBindingSDK#getDeviceFingerprint
     *
     * @return the storage fingerprint as String.
     */
    private String getSecureStorageFingerprint() {
        DeviceBindingSDKParams parameters = new DeviceBindingSDKParams(SECURE_STORAGE_SALT, this.cordova.getActivity().getApplicationContext());
        parameters.useAndroidId(true);
        try {
            return DeviceBindingSDK.getFingerprint(parameters);
        } catch (DeviceBindingSDKException e) {
            Log.e(DEVICE_BINDING_TAG, "ERROR ON GET SECURE STORAGE FINGERPRINT. CODE: " + e.getErrorCode() + "MESSAGE: " + getDeviceBindingErrorMessage(e));
        }
        return null;
    }

    /**
     * Return the storage fingerprint.
     * The storage fingerprint must be a unique identifier generated by the DeviceBindingSDK
     * DeviceBindingSDK#getDeviceFingerprint
     *
     * @return the storage fingerprint as String.
     */
    private String getDigipassFingerprint() {
        DeviceBindingSDKParams parameters = new DeviceBindingSDKParams(DIGIPASS_SALT, this.cordova.getActivity().getApplicationContext());
        parameters.useAndroidId(true);
        try {
            return DeviceBindingSDK.getFingerprint(parameters);
        } catch (DeviceBindingSDKException e) {
            Log.e(DEVICE_BINDING_TAG, "ERROR ON GET DIGIPASS FINGERPRINT. CODE: " + e.getErrorCode() + "MESSAGE: " + getDeviceBindingErrorMessage(e));
        }
        return null;
    }

    /**
     * Computes the client server time shift by using the server time and the client UTC times (timeShift = serverTime - clientTime).
     *
     * @return Client server time shift in seconds.
     */
    private long getClientServerTimeShift() {
        // return DigipassSDK.computeClientServerTimeShiftFromServerTime(12532347826l);
        return 0l;
    }

}
