//
//  Digipass.swift
//
//  Created by
//

let TAG = "DIGIPASS "
let DEVICE_BINDING_TAG = "DIGIPASS DEVICE BINDING "
let STORAGE_TAG = "DIGIPASS SECURE STORAGE "
let PLUGIN_TAG = "DIGIPASS PLUGIN "

let ACTIVATED = "ACTIVATED"
let NOT_REGISTERED = "NOT_REGISTERED"
let NOT_ACTIVATED = "NOT_ACTIVATED"
let UNKNOWN_ERROR = "UNKNOWN_ERROR"
let CORRECT_PASSWORD = "CORRECT_PASSWORD"
let PASSWORD_CHANGED = "PASSWORD_CHANGED"
let TOKEN_ACTIVATED = "TOKEN ACTIVATED SUCCESSFULLY"

let SS_SV_KEY = "245341gdf231"
let SS_DV_KEY = "234jfwno343s"

var secureStorageWrapper: SecureStorageSDKWrapper? = nil

/**
 * A salt value which can be included to the secure storage fingerprint computation.
 * Must be different for each platform (Android, iOS, Windows Phone...) and not trivial (e.g. a long random value).
 */
let SECURE_STORAGE_SALT = "3c23f88dfbf7057e6baa6105b4aa5c5fb3a920ed729c0ba6738a1b44844e95f6"

/**
 * A salt value which can be included to the device fingerprint computation.
 * Must be different for each platform (Android, iOS, Windows Phone...) and not trivial (e.g. a long random value).
 */
let DIGIPASS_SALT = "f8eaa859da36cc399327cc3cb55a591d772ece0c5de62b24bdc729eca840ff28"

/**
 * A static vector value which unique for Alinma tokens.
 */
let STATIC_VECTOR_DEFAULT = "380800E6010346444C02109D4F3D91CEEA26AD2CBC6ED38C5D611A0301010401040501040601010701010801050901040A01000B01010C01000E01000F0101100101380103112F120100130101140101150C4F545020202020202020202016010117044080F0021801002901062A01002B01002C01021135120100130101140102150C43522020202020202020202016010117044183F4D51801011901062101062901062A01002B01002C0102113B120100130101140103150C5349474E202020202020202016010117044183F4D51801021901041A01042101102201102901062A01002B01002C0102" //STATIC VALUE.

let dynamicVector: Data = Data()


@objc(PluginSwiftDemo) class Digipass: CDVPlugin {
    @objc(coolMethod:)
    func coolMethod(_ command: CDVInvokedUrlCommand) {
        let msg = command.arguments[0] as? String ?? "Error"
        print(msg)
        var pluginResult = CDVPluginResult(
            status: CDVCommandStatus_ERROR,
            messageAs: msg
        )
        if msg.count > 0 {
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: msg
            )
        }
        commandDelegate!.send(
            pluginResult,
            callbackId: command.callbackId
        )
    }
    
    @objc(initSecureStorage:)
    func initSecureStorage(_ command: CDVInvokedUrlCommand) {
        let fileName = command.arguments[0] as? String ?? "Error"
        print(fileName)
        self.commandDelegate.run(inBackground: {
            var pluginResult:CDVPluginResult? = nil
            if fileName.count > 0 {
                do {
                    secureStorageWrapper = try SecureStorageSDKWrapper(fileName: fileName, useFingerPrint: self.getSecureStorageFingerprint(), andIterationNumber: self.getIterationNumber())
                    pluginResult = CDVPluginResult(
                        status: CDVCommandStatus_OK,
                        messageAs: STORAGE_TAG + "Storage created successfully and file name is: " + fileName
                    )
                }
                catch let error as NSError {
                    print(String(error.code))
                    pluginResult = CDVPluginResult(
                        status: CDVCommandStatus_ERROR,
                        messageAs: self.buildPluginErrorResultObject(code: error.code, message: "ERROR ON SECURE STORAGE INIT, " + String(error.code))
                    )
                }
            }
            self.commandDelegate!.send(
                pluginResult,
                callbackId: command.callbackId
            )
        })
    }
    
    @objc(putStringInSecureStorage:)
    func putStringInSecureStorage(_ command: CDVInvokedUrlCommand) {
        let key = command.arguments[0] as? String ?? "Error"
        let value = command.arguments[1] as? String ?? "Error"
        self.commandDelegate.run(inBackground: {
            var pluginResult:CDVPluginResult? = nil
            
            self.storeStringInSecureStorage(key: key, value: value);
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: STORAGE_TAG + ":" + key + " was written successfully"
            )
            
            self.commandDelegate!.send(
                pluginResult,
                callbackId: command.callbackId
            )
        })
    }
    
    @objc(putByteInSecureStorage:)
    func putByteInSecureStorage(_ command: CDVInvokedUrlCommand) {
        let key = command.arguments[0] as? String ?? "Error"
        let value = command.arguments[1] as? String ?? "Error"
        self.commandDelegate.run(inBackground: {
            var pluginResult:CDVPluginResult? = nil
            
            self.storeStringInSecureStorage(key: key, value: value);
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: STORAGE_TAG + ":" + key + " was written successfully"
            )
            
            self.commandDelegate!.send(
                pluginResult,
                callbackId: command.callbackId
            )
        })
    }
    
    @objc(getStringFromSecureStorage:)
    func getStringFromSecureStorage(_ command: CDVInvokedUrlCommand) {
        let key = command.arguments[0] as? String ?? "Error"
        self.commandDelegate.run(inBackground: {
            var pluginResult:CDVPluginResult? = nil
            
            pluginResult = CDVPluginResult(
                status: CDVCommandStatus_OK,
                messageAs: self.retrieveStringFromSecureStorage(key: key)
            )
            
            self.commandDelegate!.send(
                pluginResult,
                callbackId: command.callbackId
            )
        })
    }
    
    @objc(getByteFromSecureStorage:)
    func getByteFromSecureStorage(_ command: CDVInvokedUrlCommand) {
        let key = command.arguments[0] as? String ?? "Error"
        self.commandDelegate.run(inBackground: {
            var pluginResult:CDVPluginResult? = nil
            
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: [self.retrieveByteFromSecureStorage(key: key)])
            
            self.commandDelegate!.send(
                pluginResult,
                callbackId: command.callbackId
            )
        })
    }
    
    @objc(getDigipassStatus:)
    func getDigipassStatus(_ command: CDVInvokedUrlCommand) {
        var status:String? = nil
        do {
            
            let digipassPropertiesResponse = ""
            
        }
    }
    
    
    
    private func buildPluginErrorResultObject(code: Int, message: String) -> [String: Any] {
        
        return [ "errorCode": code, "errorMessage": message ]
        
    }
    
    private func writeSecureStorage() {
        do {
            // Write secure storage to a file to save data using hardcoded fingerprint
            try secureStorageWrapper!.write(withFingerprint: getSecureStorageFingerprint(), andIterationNumber: getIterationNumber())
        } catch let error as NSError {
            print(STORAGE_TAG, "ERROR ON SECURE STORAGE WRITE. CODE" + String(error.code))
        }
        
    }
    
    private func storeStringInSecureStorage(key: String, value: String) {
        do {
            try secureStorageWrapper!.putString(forValue: value, forKey: key)
            self.writeSecureStorage()
        } catch let error as NSError {
            print(STORAGE_TAG, "ERROR ON PUT IN SECURE STORAGE. CODE" + String(error.code))
        }
    }
    
    private func storeByteInSecureStorage(key: String, value: Data) {
        do {
            try secureStorageWrapper!.putBytes(forBytes: value, forKey: key)
            self.writeSecureStorage()
        } catch let error as NSError {
            print(STORAGE_TAG, "ERROR ON PUT IN SECURE STORAGE. CODE" + String(error.code))
        }
    }
    
    private func retrieveStringFromSecureStorage(key: String) -> String {
        var value = ""
        do {
            value = try secureStorageWrapper!.getStringForKey(key)
        } catch  let error as NSError {
            print(STORAGE_TAG, "ERROR ON SECURE STORAGE GET. CODE" + String(error.code))
        }
        return value
    }
    
    private func retrieveByteFromSecureStorage(key: String) -> Data {
        var value: Data = Data()
        do {
            value = try secureStorageWrapper!.getBytesForKey(key)
        } catch let error as NSError {
            print(STORAGE_TAG, "ERROR ON SECURE STORAGE GET. CODE" + String(error.code))
        }
        return value
    }
    
    
    
    private func getSecureStorageFingerprint() -> String {
        
        var deviceFingerprint = ""
        
        do {
            // Create and get the device Fingerprint with a sample salt
            deviceFingerprint = try DeviceBindingSDKWrapper.getDeviceFingerPrint(withDynamicSalt: SECURE_STORAGE_SALT)
        }
        catch let error as NSError {
            // Show the Exception's reason
            print(error.userInfo["message"] as? String)
        }
        
        return deviceFingerprint
    }
    
    private func getIterationNumber() -> Int32 {
        return 300
    }
}
