
#import "Digipass.h"
#import <Cordova/CDVPlugin.h>

@implementation NSString (ASCII)

+ (NSString *)stringWithAscii:(vds_ascii *)ascii {
    return [NSString stringWithCString:ascii encoding:NSUTF8StringEncoding];
}

- (vds_ascii *)ascii {
    return (vds_ascii*) [self cStringUsingEncoding:NSUTF8StringEncoding];
}

@end

@implementation NSData (VDSByte)

- (vds_byte *)vdsBytes
{
    return (vds_byte *)self.bytes;
}

- (vds_int32)vdsLength
{
    // sdk use sizeof()
    return (vds_int32) self.length;
}

@end

@implementation Digipass

SecureStorageSDK *_secureStorage;

static NSString *const kDomain = @"SecureStorageSDK";
static NSString *const kMessage = @"message";

static NSString *const TAG = @"DIGIPASS:";
static NSString *const DEVICE_BINDING_TAG = @"DIGIPASS DEVICE BINDING:";
static NSString *const STORAGE_TAG = @"DIGIPASS SECURE STORAGE:";
static NSString *const PLUGIN_TAG = @"DIGIPASS PLUGIN:";

static NSString *const ACTIVATED = @"ACTIVATED";
static NSString *const NOT_REGISTERED = @"NOT_REGISTERED";
static NSString *const NOT_ACTIVATED = @"NOT_ACTIVATED";
static NSString *const UNKNOWN_ERROR = @"UNKNOWN_ERROR";
static NSString *const CORRECT_PASSWORD = @"CORRECT_PASSWORD";
static NSString *const PASSWORD_CHANGED = @"PASSWORD_CHANGED";
static NSString *const TOKEN_ACTIVATED = @"TOKEN ACTIVATED SUCCESSFULLY";

static NSString *const SS_SV_KEY = @"245341gdf231"; // recommended to be changed to code
static NSString *const SS_DV_KEY = @"234jfwno343s";


static NSString *const SECURE_STORAGE_SALT = @"3c23f88dfbf7057e6baa6105b4aa5c5fb3a920ed729c0ba6738a1b44844e95f6";
static NSString *const DIGIPASS_SALT = @"f8eaa859da36cc399327cc3cb55a591d772ece0c5de62b24bdc729eca840ff28";

static NSString *const  STATIC_VECTOR_DEFAULT = @"380800E6010346444C02109D4F3D91CEEA26AD2CBC6ED38C5D611A0301010401040501040601010701010801050901040A01000B01010C01000E01000F0101100101380103112F120100130101140101150C4F545020202020202020202016010117044080F0021801002901062A01002B01002C01021135120100130101140102150C43522020202020202020202016010117044183F4D51801011901062101062901062A01002B01002C0102113B120100130101140103150C5349474E202020202020202016010117044183F4D51801021901041A01042101102201102901062A01002B01002C0102"; //STATIC VALUE.

vds_byte staticVectorResponse[LENGTH_STATIC_VECTOR_V8_MAX];
vds_byte dynamicVector[LENGTH_DYNAMIC_VECTOR_V8_MAX];
vds_int32 staticVectorLength = sizeof(staticVectorResponse);
vds_int32 dynamicVectorLength = sizeof(dynamicVector);

vds_byte* dynamicVectorResponse;
vds_int32 dynamicVectorResponseLength;

vds_byte encryptionKey[] = {0x05, 0x02, 0x01, 0x00, 0x07, 0x03, 0x07, 0x08, 0x09, 0x03, 0x09, 0x02, 0x03, 0x04, 0x07, 0x09};
vds_int32 encryptionKeyLength = sizeof(encryptionKey);

-(int) getIterationNumber
{
    return 300;
}

-(long) getClientServerTimeShift
{
    return 0;
}

-(NSString *) getDigipassFingerprint
{
    @try {
        return [DeviceBindingSDK getDeviceFingerPrintWithDynamicSalt:DIGIPASS_SALT];
    }
    @catch (DeviceBindingSDKException *e)
    {
        NSString * message = e.exception ? e.exception.reason : @"";
        NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
        NSLog(@"%@", error);
        return nil;
    }
}


-(NSString *) getSecureStorageFingerprint
{
    @try {
        return [DeviceBindingSDK getDeviceFingerPrintWithDynamicSalt:SECURE_STORAGE_SALT];
    }
    @catch (DeviceBindingSDKException *e)
    {
        NSString * message = e.exception ? e.exception.reason : @"";
        NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
        NSLog(@"%@", error);
        return nil;
    }
}

- (void)writeSecureStorage
{
    @try {
        [_secureStorage writeWithFingerPrint:[self getSecureStorageFingerprint] andIterationNumber:[self getIterationNumber]];
    }
    @catch (SecureStorageSDKException * e)
    {
        NSString * message = e.exception ? e.exception.reason : @"";
        NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
        NSLog(@"%@", error);
    }
}

- (void)putStringInSecureStorageForKey:(NSString *)key value:(NSString *)value
{
    @try {
        [_secureStorage putString:value forKey:key];
        [self writeSecureStorage];
    }
    @catch (SecureStorageSDKException * e)
    {
        NSString * message = e.exception ? e.exception.reason : @"";
        NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
        NSLog(@"%@", error);
    }
}

- (void)putByteInSecureStorageForKey:(NSString *)key value:(NSData *)value
{
    @try {
        [_secureStorage putBytes:value forKey:key];
        [self writeSecureStorage];
    }
    @catch (SecureStorageSDKException * e)
    {
        NSString * message = e.exception ? e.exception.reason : @"";
        NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
        NSLog(@"%@", error);
    }
}

- (NSString *_Nullable)getStringFromSecureStorageForKey:(NSString *)key
{
    @try {
        return [_secureStorage getStringForKey:key];
    }
    @catch (SecureStorageSDKException * e)
    {
        NSString * message = e.exception ? e.exception.reason : @"";
        NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
        NSLog(@"%@", error);
    }
}

- (NSData *_Nullable)getByteFromSecureStorageForKey:(NSString *)key
{
    @try {
        if([_secureStorage containsKey:key])
        {
            return [_secureStorage getBytesForKey:key];
        }
        else
        {
            return nil;
        }
    }
    @catch (SecureStorageSDKException * e)
    {
        NSString * message = e.exception ? e.exception.reason : @"";
        NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
        NSLog(@"%@", error);
    }
}

- (void)removeFromSecureStorageForKey: (NSString *)key
{
    @try {
        [_secureStorage removeForKey:key];
    }
    @catch (SecureStorageSDKException * e)
    {
        NSString * message = e.exception ? e.exception.reason : @"";
        NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
        NSLog(@"%@", error);
    }
}

- (NSString *_Nullable)composeStringByArray: (NSArray*)array
{
    return [array componentsJoinedByString:@" "];
}

- (NSDictionary *_Nullable)buildPluginErrorResultCode: (long)code andMessage: (NSString *)message
{
    NSDictionary *dict = nil;
    
    dict = @{ @"errorCode" : [NSString stringWithFormat:@"%ld", code], @"errorMessage" : message};
    
    return dict;
    
}

- (void)initSecureStorage:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* fileName = [command.arguments objectAtIndex:0];
        if (fileName != nil && [fileName length] > 0) {
            @try {
                _secureStorage = [[SecureStorageSDK alloc] initWithFileName:fileName useFingerPrint:[self getSecureStorageFingerprint] andIterationNumber:[self getIterationNumber]];
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: [self composeStringByArray:@[STORAGE_TAG, @"Storage created successfully and file name is:", fileName]]];
            }
            @catch (SecureStorageSDKException *e) {
                NSString * message = e.exception ? e.exception.reason : @"";
                NSError * error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
                
                NSLog(@"%@", [self composeStringByArray:@[STORAGE_TAG, @"ERROR ON SECURE STORAGE INIT,", error.userInfo]]);
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary: [self buildPluginErrorResultCode:e.errorCode andMessage:[self composeStringByArray:@[STORAGE_TAG, @"ERROR ON SECURE STORAGE INIT,", error.userInfo]]]];
            }
        } else {
            NSLog(@"%@", [self composeStringByArray:@[STORAGE_TAG, @"ERROR ON SECURE STORAGE INIT,", @"no file name provided"]]);
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[self composeStringByArray:@[STORAGE_TAG, @"ERROR ON SECURE STORAGE INIT,", @"no file name provided"]]];
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)putStringInSecureStorage:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* key = [command.arguments objectAtIndex:0];
        NSString* value = [command.arguments objectAtIndex:1];
        
        @try {
            [self putStringInSecureStorageForKey:key value:value];
            [self writeSecureStorage];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: [self composeStringByArray:@[STORAGE_TAG, key, @"was written successfully"]]];
        }
        @catch (SecureStorageSDKException *e) {
            NSString * message = e.exception ? e.exception.reason : @"";
            NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
            
            NSLog(@"%@", [self composeStringByArray:@[STORAGE_TAG, @"ERROR ON PUT IN SECURE STORAGE. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]);
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary: [self buildPluginErrorResultCode:e.errorCode andMessage:[self composeStringByArray:@[STORAGE_TAG, @"ERROR ON PUT IN SECURE STORAGE. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]]];
        }
        
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)putByteInSecureStorage:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* key = [command.arguments objectAtIndex:0];
        NSData* value = [command.arguments objectAtIndex:1];
        
        @try {
            [self putByteInSecureStorageForKey:key value:value];
            [self writeSecureStorage];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: [self composeStringByArray:@[STORAGE_TAG, key, @"was written successfully"]]];
        }
        @catch (SecureStorageSDKException *e) {
            NSString * message = e.exception ? e.exception.reason : @"";
            NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
            
            NSLog(@"%@", [self composeStringByArray:@[STORAGE_TAG, @"ERROR ON PUT IN SECURE STORAGE. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]);
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary: [self buildPluginErrorResultCode:e.errorCode andMessage:[self composeStringByArray:@[STORAGE_TAG, @"ERROR ON PUT IN SECURE STORAGE. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]]];
        }
        
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)getStringFromSecureStorage:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* key = [command.arguments objectAtIndex:0];
        
        @try {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: [self getStringFromSecureStorageForKey:key]];
        }
        @catch (SecureStorageSDKException *e) {
            NSString * message = e.exception ? e.exception.reason : @"";
            NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
            
            NSLog(@"%@", [self composeStringByArray:@[STORAGE_TAG, @"ERROR ON SECURE STORAGE GET. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]);
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary: [self buildPluginErrorResultCode:e.errorCode andMessage:[self composeStringByArray:@[STORAGE_TAG, @"ERROR ON SECURE STORAGE GET. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]]];
        }
        
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)getByteFromSecureStorage:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* key = [command.arguments objectAtIndex:0];
        
        @try {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArrayBuffer: [self getByteFromSecureStorageForKey:key]];
        }
        @catch (SecureStorageSDKException *e) {
            NSString * message = e.exception ? e.exception.reason : @"";
            NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
            
            NSLog(@"%@", [self composeStringByArray:@[STORAGE_TAG, @"ERROR ON SECURE STORAGE GET. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]);
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary: [self buildPluginErrorResultCode:e.errorCode andMessage:[self composeStringByArray:@[STORAGE_TAG, @"ERROR ON SECURE STORAGE GET. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]]];
        }
        
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)deactivateToken:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        
        @try {
            [self removeFromSecureStorageForKey:SS_DV_KEY];
            [self writeSecureStorage];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        }
        @catch (SecureStorageSDKException *e) {
            NSString * message = e.exception ? e.exception.reason : @"";
            NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
            
            NSLog(@"%@", [self composeStringByArray:@[STORAGE_TAG, @"ERROR ON SECURE STORAGE REMOVE. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]);
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary: [self buildPluginErrorResultCode:e.errorCode andMessage:[self composeStringByArray:@[STORAGE_TAG, @"ERROR ON SECURE STORAGE REMOVE. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]]];
        }
        
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)getDigipassStatus:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* status = nil;
        
        @try {
            if (![_secureStorage containsKey:SS_SV_KEY])
            {
                status = NOT_REGISTERED;
            }
            else
            {
                
                vds_byte value;
                NSData * SV = [self getByteFromSecureStorageForKey:SS_SV_KEY];
                NSData * DV = [self getByteFromSecureStorageForKey:SS_DV_KEY];
                
                vds_int32 returnCode = DPSDK_GetDigipassProperty(SV.vdsBytes, SV.vdsLength, DV.vdsBytes, DV.vdsLength, PROPERTY_STATUS, &value, sizeof(value), 0);
                
                if ((int)returnCode != RETURN_CODE_SUCCESS)
                {
                    NSLog(@"%@", [NSString stringWithAscii: DPSDK_GetMessageForReturnCode(returnCode)]);
                    status = NOT_ACTIVATED;
                }
                else
                {
                    status = ACTIVATED;
                }
            }
            
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:status];
        }
        @catch (SecureStorageSDKException *e) {
            NSString * message = e.exception ? e.exception.reason : @"";
            NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
            
            NSLog(@"%@", [self composeStringByArray:@[STORAGE_TAG, @"ERROR ON GET DIGIPASS STATUS. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]);
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary: [self buildPluginErrorResultCode:e.errorCode andMessage:[self composeStringByArray:@[STORAGE_TAG, @"ERROR ON GET DIGIPASS STATUS. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]]];
        }
        
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}


- (void)getDigipassProperties:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* status = nil;
        NSDictionary *dict = nil;
        
        @try {
            if (![_secureStorage containsKey:SS_SV_KEY])
            {
                status = NOT_REGISTERED;
            }
            else
            {
                vds_byte value;
                NSData * SV = [self getByteFromSecureStorageForKey:SS_SV_KEY];
                NSData * DV = [self getByteFromSecureStorageForKey:SS_DV_KEY];
                
                vds_int32 returnCode = DPSDK_GetDigipassProperty(SV.vdsBytes, SV.vdsLength, DV.vdsBytes, DV.vdsLength, PROPERTY_STATUS, &value, sizeof(value), 0);
                
                if ((int)returnCode != RETURN_CODE_SUCCESS)
                {
                    status = NOT_ACTIVATED;
                }
                else
                {
                    status = ACTIVATED;
                    vds_ascii* serialNumber;
                    int serialNumberLength = LENGTH_SERIAL_NUMBER + 1;
                    vds_int32 phoneTime;
                    
                    serialNumber = (vds_ascii*)malloc(sizeof(vds_ascii) * serialNumberLength);
                    memset(serialNumber, 0, serialNumberLength);
                    
                    vds_int32 serialNumberReturnCode = DPSDK_GetDigipassProperty(SV.vdsBytes, SV.vdsLength, DV.vdsBytes, DV.vdsLength, PROPERTY_SERIAL_NUMBER, serialNumber, (vds_int32)serialNumberLength, 0);
                    vds_int32 phoneTimeReturnCode = DPSDK_GetDigipassProperty(SV.vdsBytes, SV.vdsLength, DV.vdsBytes, DV.vdsLength, PROPERTY_UTC_TIME, &phoneTime, sizeof(phoneTime), 0);
                    
                    
                    if (serialNumberReturnCode != RETURN_CODE_SUCCESS)
                    {
                        NSLog(@"%@", [NSString stringWithAscii: DPSDK_GetMessageForReturnCode(serialNumberReturnCode)]);
                    }
                    
                    if (phoneTimeReturnCode != RETURN_CODE_SUCCESS)
                    {
                        NSLog(@"%@", [NSString stringWithAscii: DPSDK_GetMessageForReturnCode(phoneTimeReturnCode)]);
                    }
                    
                    
                    dict =  @{ @"serialNumber" : [NSString stringWithAscii:serialNumber], @"phoneTime" : @(phoneTime), @"timeShift" : [NSNumber numberWithLong:[self getClientServerTimeShift]]};
                    
                    free(serialNumber);
                }
            }
            if (status == ACTIVATED) {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict];
            }else{
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:status];
            }
            
        }
        @catch (SecureStorageSDKException *e) {
            NSString * message = e.exception ? e.exception.reason : @"";
            NSError *error = [[NSError alloc] initWithDomain:kDomain code:e.errorCode userInfo:@{kMessage:message}];
            
            NSLog(@"%@", [self composeStringByArray:@[STORAGE_TAG, @"ERROR ON GET DIGIPASS PROPERTIES. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]);
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary: [self buildPluginErrorResultCode:e.errorCode andMessage:[self composeStringByArray:@[STORAGE_TAG, @"ERROR ON GET DIGIPASS PROPERTIES. CODE", [NSString stringWithFormat:@"%i", e.errorCode], @", MESSAGE", error.userInfo]]]];
        }
        
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)activateToken:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* serialNumber = [command.arguments objectAtIndex:0];
        NSString* activationCode = [command.arguments objectAtIndex:1];
        NSString* eventReactivationCounter = [command.arguments objectAtIndex:2];
        NSString* password = [command.arguments objectAtIndex:3];
        
        vds_int32 returnCode;
        
        returnCode = DPSDK_ActivateOfflineWithFingerprint(STATIC_VECTOR_DEFAULT.ascii, serialNumber.ascii, activationCode.ascii, eventReactivationCounter.ascii, NULL, password.ascii, [self getDigipassFingerprint].ascii, staticVectorResponse, &staticVectorLength, dynamicVector, &dynamicVectorLength);
        NSData* staticVectorData = [NSData dataWithBytes:staticVectorResponse length:staticVectorLength];
        NSData* dynamicVectorData = [NSData dataWithBytes:dynamicVector length:dynamicVectorLength];
        
        if (returnCode != RETURN_CODE_SUCCESS)
        {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:[self buildPluginErrorResultCode:returnCode andMessage:[self composeStringByArray:@[PLUGIN_TAG, [NSString stringWithAscii:DPSDK_GetMessageForReturnCode(returnCode)]]]]];
        }
        else
        {
            [self putByteInSecureStorageForKey:SS_SV_KEY value: staticVectorData];
            [self putByteInSecureStorageForKey:SS_DV_KEY value: dynamicVectorData];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:TOKEN_ACTIVATED];
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)generateOTP:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* password = [command.arguments objectAtIndex:0];
        
        vds_int32 returnCode;
        
        NSData * SV = [self getByteFromSecureStorageForKey:SS_SV_KEY];
        NSData * DV = [self getByteFromSecureStorageForKey:SS_DV_KEY];
        
        NSData* dynamicVectorData = [NSData dataWithBytes:DV.vdsBytes length:DV.vdsLength];
        
        
        vds_int32 dvLength = 0;
        returnCode = DPSDK_GetDynamicVectorLength(SV.vdsBytes, SV.vdsLength, &dvLength);
        if (returnCode != RETURN_CODE_SUCCESS) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:[self buildPluginErrorResultCode:returnCode andMessage:[self composeStringByArray:@[PLUGIN_TAG, [NSString stringWithAscii:DPSDK_GetMessageForReturnCode(returnCode)]]]]];
        }
        else
        {
            vds_byte *dvBytes = (vds_byte *) malloc(sizeof(vds_byte) * dvLength);
            memset(dvBytes, 0, dvLength);
            [dynamicVectorData getBytes:dvBytes length:dynamicVectorData.length];
            
            // Initialize response arrays
            vds_ascii response[LENGTH_RESPONSE_MAX + 1];
            vds_ascii hostCode[LENGTH_HOST_CODE_MAX + 1];
            memset(response, 0, sizeof(response));
            memset(hostCode, 0, sizeof(hostCode));
            
            returnCode = DPSDK_GenerateResponseOnly(SV.vdsBytes, SV.vdsLength,
                                                    dvBytes, dvLength,
                                                    password.ascii,
                                                    (vds_int32)[self getClientServerTimeShift],
                                                    (vds_int32)CRYPTO_APPLICATION_INDEX_APP_1,
                                                    [self getDigipassFingerprint].ascii,
                                                    response, sizeof(response),
                                                    hostCode, sizeof(hostCode));
            if(returnCode != RETURN_CODE_SUCCESS)
            {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:[self buildPluginErrorResultCode:returnCode andMessage:[self composeStringByArray:@[PLUGIN_TAG, [NSString stringWithAscii:DPSDK_GetMessageForReturnCode(returnCode)]]]]];
            }
            else
            {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[NSString stringWithAscii: response]];
            }
            
            
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }];
    
}

- (void)validatePassword:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* password = [command.arguments objectAtIndex:0];
        
        vds_int32 returnCode;
        
        NSData * SV = [self getByteFromSecureStorageForKey:SS_SV_KEY];
        NSData * DV = [self getByteFromSecureStorageForKey:SS_DV_KEY];
        
        NSData* dynamicVectorData = [NSData dataWithBytes:DV.vdsBytes length:DV.vdsLength];
        
        
        vds_int32 dvLength = 0;
        returnCode = DPSDK_GetDynamicVectorLength(SV.vdsBytes, SV.vdsLength, &dvLength);
        if (returnCode != RETURN_CODE_SUCCESS) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:[self buildPluginErrorResultCode:returnCode andMessage:[self composeStringByArray:@[PLUGIN_TAG, [NSString stringWithAscii:DPSDK_GetMessageForReturnCode(returnCode)]]]]];
            
        }
        else
        {
            vds_byte *dvBytes = (vds_byte *) malloc(sizeof(vds_byte) * dvLength);
            memset(dvBytes, 0, dvLength);
            [dynamicVectorData getBytes:dvBytes length:dynamicVectorData.length];
            
            // Initialize response arrays
            vds_ascii response[LENGTH_RESPONSE_MAX + 1];
            vds_ascii hostCode[LENGTH_HOST_CODE_MAX + 1];
            memset(response, 0, sizeof(response));
            memset(hostCode, 0, sizeof(hostCode));
            
            returnCode = DPSDK_ValidatePasswordWithFingerprint(SV.vdsBytes, SV.vdsLength,
                                                               dvBytes, dvLength,
                                                               password.ascii,
                                                               [self getDigipassFingerprint].ascii,
                                                               encryptionKey, encryptionKeyLength);
            if(returnCode != RETURN_CODE_SUCCESS)
            {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:[self buildPluginErrorResultCode:returnCode andMessage:[self composeStringByArray:@[PLUGIN_TAG, [NSString stringWithAscii:DPSDK_GetMessageForReturnCode(returnCode)]]]]];
            }
            else
            {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: CORRECT_PASSWORD];
            }
            NSData *newDynamicVector = [NSData dataWithBytes:dvBytes length:dvLength];
            [self putByteInSecureStorageForKey:SS_DV_KEY value: newDynamicVector];
            free(dvBytes);
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }];
    
}

- (void)changePassword:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* oldPassword = [command.arguments objectAtIndex:0];
        NSString* newPassword = [command.arguments objectAtIndex:1];
        
        vds_int32 returnCode;
        
        NSData * SV = [self getByteFromSecureStorageForKey:SS_SV_KEY];
        NSData * DV = [self getByteFromSecureStorageForKey:SS_DV_KEY];
        
        NSData* dynamicVectorData = [NSData dataWithBytes:DV.vdsBytes length:DV.vdsLength];
        
        vds_int32 dvLength = 0;
        returnCode = DPSDK_GetDynamicVectorLength(SV.vdsBytes, SV.vdsLength, &dvLength);
        if (returnCode != RETURN_CODE_SUCCESS) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:[self buildPluginErrorResultCode:returnCode andMessage:[self composeStringByArray:@[PLUGIN_TAG, [NSString stringWithAscii:DPSDK_GetMessageForReturnCode(returnCode)]]]]];
            
        }
        else
        {
            vds_byte *dvBytes = (vds_byte *) malloc(sizeof(vds_byte) * dvLength);
            memset(dvBytes, 0, dvLength);
            [dynamicVectorData getBytes:dvBytes length:dynamicVectorData.length];
            
            // Initialize response arrays
            vds_ascii response[LENGTH_RESPONSE_MAX + 1];
            vds_ascii hostCode[LENGTH_HOST_CODE_MAX + 1];
            memset(response, 0, sizeof(response));
            memset(hostCode, 0, sizeof(hostCode));
            
            returnCode = DPSDK_ChangePasswordWithFingerprint(SV.vdsBytes, SV.vdsLength,
                                                             dvBytes, dvLength,
                                                             oldPassword.ascii, newPassword.ascii,
                                                             [self getDigipassFingerprint].ascii);
            if(returnCode != RETURN_CODE_SUCCESS)
            {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:[self buildPluginErrorResultCode:returnCode andMessage:[self composeStringByArray:@[PLUGIN_TAG, [NSString stringWithAscii:DPSDK_GetMessageForReturnCode(returnCode)]]]]];
            }
            else
            {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: PASSWORD_CHANGED];
            }
            NSData *newDynamicVector = [NSData dataWithBytes:dvBytes length:dvLength];
            [self putByteInSecureStorageForKey:SS_DV_KEY value: newDynamicVector];
            free(dvBytes);
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }];
    
}

- (void)isPasswordWeak:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* password = [command.arguments objectAtIndex:0];
        
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool: DPSDK_IsPasswordWeak(password.ascii)];
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }];
    
}

- (void)generateSignature:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* password = [command.arguments objectAtIndex:0];
        NSArray<NSString *>* dataFields = (NSArray<NSString *>*)[command.arguments objectAtIndex:1];
        // [NSJSONSerialization JSONObjectWithData:[command.arguments objectAtIndex:1] options:NSJSONReadingMutableContainers error:nil];
        
        vds_int32 returnCode;
        NSData * SV = [self getByteFromSecureStorageForKey:SS_SV_KEY];
        NSData * DV = [self getByteFromSecureStorageForKey:SS_DV_KEY];
        
        NSData* dynamicVectorData = [NSData dataWithBytes:DV.vdsBytes length:DV.vdsLength];
        
        vds_int32 dvLength = 0;
        returnCode = DPSDK_GetDynamicVectorLength(SV.vdsBytes, SV.vdsLength, &dvLength);
        if (returnCode != RETURN_CODE_SUCCESS) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:[self buildPluginErrorResultCode:returnCode andMessage:[self composeStringByArray:@[PLUGIN_TAG, [NSString stringWithAscii:DPSDK_GetMessageForReturnCode(returnCode)]]]]];
            
        }
        else
        {
            vds_byte *dvBytes = (vds_byte *) malloc(sizeof(vds_byte) * dvLength);
            memset(dvBytes, 0, dvLength);
            [dynamicVectorData getBytes:dvBytes length:dynamicVectorData.length];
            
            // Initialize response arrays
            vds_ascii response[LENGTH_RESPONSE_MAX + 1];
            vds_ascii hostCode[LENGTH_HOST_CODE_MAX + 1];
            memset(response, 0, sizeof(response));
            memset(hostCode, 0, sizeof(hostCode));
            
            vds_int32 dataFieldsNumber = (vds_int32)dataFields.count;
            vds_ascii dataFieldsC[NUMBER_DATA_FIELD_MAX][LENGTH_DATA_FIELD_MAX + 1];
            memset(dataFieldsC, 0, sizeof(dataFieldsC));
            
            NSUInteger numberOfFields = dataFields.count > NUMBER_DATA_FIELD_MAX ? NUMBER_DATA_FIELD_MAX : dataFields.count;
            
            for (int i = 0; i < numberOfFields; i++) {
                vds_ascii *field = (dataFields[i].ascii);
                memcpy(dataFieldsC[i], field, strlen(field));
            }
            
            returnCode = DPSDK_GenerateSignature(SV.vdsBytes, SV.vdsLength,
                                                 dvBytes, dvLength, dataFieldsC, dataFieldsNumber,
                                                 password.ascii, [self getClientServerTimeShift], CRYPTO_APPLICATION_INDEX_APP_3,
                                                 [self getDigipassFingerprint].ascii, response, sizeof(response),
                                                 hostCode, sizeof(hostCode));
            if(returnCode != RETURN_CODE_SUCCESS)
            {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:[self buildPluginErrorResultCode:returnCode andMessage:[self composeStringByArray:@[PLUGIN_TAG, [NSString stringWithAscii:DPSDK_GetMessageForReturnCode(returnCode)]]]]];
            }
            else
            {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: [NSString stringWithAscii: response]];
            }
            NSData *newDynamicVector = [NSData dataWithBytes:dvBytes length:dvLength];
            [self putByteInSecureStorageForKey:SS_DV_KEY value: newDynamicVector];
            free(dvBytes);
        }
        
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        
    }];
    
}


@end
