
#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
#import <SecureStorageSDK/SecureStorageSDK.h>
#import <SecureStorageSDK/SecureStorageSDKErrorCodes.h>
#import <SecureStorageSDK/SecureStorageSDKException.h>
#import <DeviceBindingSDK/DeviceBindingSDK.h>
#import <DeviceBindingSDK/DeviceBindingSDKException.h>
#import <DigipassSDK/DP4Capi.h>
#import <string.h>
#import <stdlib.h>


@interface NSString (ASCII)

+ (NSString *)stringWithAscii:(vds_ascii *)ascii;
- (vds_ascii *)ascii;

@end

@interface NSData (VDSByte)

- (vds_byte *)vdsBytes;
- (vds_int32)vdsLength;

@end

@interface Digipass : CDVPlugin

- (void)initSecureStorage:(CDVInvokedUrlCommand*)command;
- (void)putStringInSecureStorage:(CDVInvokedUrlCommand*)command;
- (void)putByteInSecureStorage:(CDVInvokedUrlCommand*)command;
- (void)getStringFromSecureStorage:(CDVInvokedUrlCommand*)command;
- (void)getByteFromSecureStorage:(CDVInvokedUrlCommand*)command;
- (void)getDigipassStatus:(CDVInvokedUrlCommand*)command;
- (void)getDigipassProperties:(CDVInvokedUrlCommand*)command;
- (void)activateToken:(CDVInvokedUrlCommand*)command;
- (void)generateOTP:(CDVInvokedUrlCommand*)command;
- (void)validatePassword:(CDVInvokedUrlCommand*)command;
- (void)changePassword:(CDVInvokedUrlCommand*)command;
- (void)isPasswordWeak:(CDVInvokedUrlCommand*)command;
- (void)generateSignature:(CDVInvokedUrlCommand*)command;
- (void)deactivateToken:(CDVInvokedUrlCommand*)command;

@end
