var argscheck = require('cordova/argscheck');
var channel = require('cordova/channel');
var exec = require('cordova/exec');
var cordova = require('cordova');

/**
 * This represents the mobile device, and provides properties for inspecting the model, version, UUID of the
 * phone, etc.
 * @constructor
 */
function Digipass() {
}


Digipass.prototype.coolMethod = function (string, successCallback, errorCallback) {
    argscheck.checkArgs('s', 'Digipass.coolMethod', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'coolMethod', [string]);
};

/**
 * Init Secure Storage
 *
 * @param {String} fileName Storage file name. Must not be null nor empty, the max length is FILENAME_MAX_LENGTH and it can only contain alphanumeric characters (along with "-", "_" and ".")
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.initSecureStorage = function (fileName, successCallback, errorCallback) {
    argscheck.checkArgs('sFF', 'Digipass.initSecureStorage', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'initSecureStorage', [fileName]);
};

/**
 * Put String In SecureStorage
 *
 * @param {String} code Used to identify a storage value. Must not be null nor empty, the max length is STORAGE_KEY_MAX_LENGTH and it can only contain alphanumeric characters
 * @param {String} value String to store. Must not be null and cannot contain characters "¨" and "§"
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.putStringInSecureStorage = function (code, value, successCallback, errorCallback) {
    argscheck.checkArgs('ssFF', 'Digipass.putStringInSecureStorage', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'putStringInSecureStorage', [code, value]);
};

/**
 * Get String From SecureStorage
 *
 * @param {String} code Key to retrieve value. Must not be null, must not be empty, key.length() must be <= STORAGE_KEY_MAX_LENGTH and can only contain alphanumeric characters
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.getStringFromSecureStorage = function (code, successCallback, errorCallback) {
    argscheck.checkArgs('sFF', 'Digipass.getStringFromSecureStorage', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'getStringFromSecureStorage', [code]);
};

/**
 * Put Byte In SecureStorage
 *
 * @param {String} code Used to identify a storage value. Must not be null nor empty, the max length is STORAGE_KEY_MAX_LENGTH and it can only contain alphanumeric characters
 * @param {String} value Bytes array to store. Must not be null
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.putByteInSecureStorage = function (code, value, successCallback, errorCallback) {
    argscheck.checkArgs('ssFF', 'Digipass.putByteInSecureStorage', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'putByteInSecureStorage', [code, value]);
};

/**
 * Get Byte From SecureStorage
 *
 * @param {String} code Key to retrieve value. Must not be null, must not be empty, key.length() must be <= STORAGE_KEY_MAX_LENGTH and can only contain alphanumeric characters
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.getByteFromSecureStorage = function (code, successCallback, errorCallback) {
    argscheck.checkArgs('sFF', 'Digipass.getByteFromSecureStorage', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'getByteFromSecureStorage', [code]);
};

/**
 * Deactivate Token
 *
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
 Digipass.prototype.deactivateToken = function (successCallback, errorCallback) {
    argscheck.checkArgs('fF', 'Digipass.deactivate', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'deactivateToken', []);
};

/**
 * Get Digipass Status
 *
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.getDigipassStatus = function (successCallback, errorCallback) {
    argscheck.checkArgs('fF', 'Digipass.getDigipassStatus', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'getDigipassStatus', []);
};

/**
 * Get Digipass Properties
 *
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.getDigipassProperties = function (successCallback, errorCallback) {
    argscheck.checkArgs('fF', 'Digipass.getDigipassProperties', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'getDigipassProperties', []);
};

/**
 * Activate Token
 *
 * @param {String} serialNumber Serial number, 10 characters. MANDATORY. A serial number suffix can also be accepted (i.e. the 7 last characters of the serial number). In this case, the serial number prefix will be retrieved from the static vector
 * @param {String} activationCode Activation code, used to activate the DIGIPASS, MANDATORY, from 20 to 41 decimal or hexadecimal characters according to the DIGIPASS settings (see getDigipassProperties(byte[], byte[]), DigipassPropertiesResponse.isActivationCodeFormatHexa() and DigipassPropertiesResponse.isUseChecksumForActivationCode())
 * @param {String} eventReactivationCounter Event reactivation counter, contains the applications counters, OPTIONAL, only required for the reactivation of an event-based DIGIPASS, else must be null, from 3 to 91 decimal or hexadecimal characters according to the DIGIPASS settings (see getDigipassProperties(byte[], byte[]) and DigipassPropertiesResponse.isActivationCodeFormatHexa())
 * @param {String} password Password used to encrypt DIGIPASS secrets, MANDATORY if the password is required in the DIGIPASS settings (see getDigipassProperties(byte[], byte[]) and DigipassPropertiesResponse.isPasswordMandatory())
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.activateToken = function (serialNumber, activationCode, eventReactivationCounter, password, successCallback, errorCallback) {
    argscheck.checkArgs('ssssFF', 'Digipass.activateToken', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'activateToken', [serialNumber, activationCode, eventReactivationCounter, password]);
};

/**
 * Generate OTP
 *
 * @param {String} password Password used to decrypt DIGIPASS secrets, MANDATORY if the password is required in the DIGIPASS settings (see getDigipassProperties(byte[], byte[]) and DigipassPropertiesResponse.isPasswordProtected())
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.generateOTP = function (password, successCallback, errorCallback) {
    argscheck.checkArgs('sFF', 'Digipass.generateOTP', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'generateOTP', [password]);
};

/**
 * Validate Password
 *
 * @param {String} password Password used to decrypt DIGIPASS secrets, MANDATORY if the password is required in the DIGIPASS settings (see getDigipassProperties(byte[], byte[]) and DigipassPropertiesResponse.isPasswordProtected())
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.validatePassword = function (password, successCallback, errorCallback) {
    argscheck.checkArgs('sFF', 'Digipass.validatePassword', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'validatePassword', [password]);
};

/**
 * Change Password
 *
 * @param {String} oldPassword Old password used to decrypt DIGIPASS secrets, MANDATORY if the password is required in the DIGIPASS settings (see getDigipassProperties(byte[], byte[]) and DigipassPropertiesResponse.isPasswordProtected())
 *  * @param {String} newPassword New password used to encrypt DIGIPASS secrets, MANDATORY if the password is required in the DIGIPASS settings (see getDigipassProperties(byte[], byte[]) and DigipassPropertiesResponse.isPasswordProtected())
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.changePassword = function (oldPassword, newPassword, successCallback, errorCallback) {
    argscheck.checkArgs('ssFF', 'Digipass.changePassword', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'changePassword', [oldPassword, newPassword]);
};

/**
 * Is Password Weak
 *
 * @param {String} password Password used to decrypt DIGIPASS secrets, MANDATORY if the password is required in the DIGIPASS settings (see getDigipassProperties(byte[], byte[]) and DigipassPropertiesResponse.isPasswordProtected())
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.isPasswordWeak = function (password, successCallback, errorCallback) {
    argscheck.checkArgs('sFF', 'Digipass.isPasswordWeak', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'isPasswordWeak', [password]);
};

/**
 * Generate Signature
 *
 * @param {String} password Password used to decrypt DIGIPASS secrets, MANDATORY if the password is required in the DIGIPASS settings (see getDigipassProperties(byte[], byte[]) and DigipassPropertiesResponse.isPasswordProtected())
 * @param {String[]} dataFields Data fields to sign, up to 8 strings having up to 16 valid characters, the array length must not exceed the data fields number contained in the DIGIPASS settings (see getDigipassProperties(byte[], byte[]), DigipassPropertiesResponse.Application.getDataFieldNumber(), DigipassPropertiesResponse.Application.getDataFieldsMinLength() and DigipassPropertiesResponse.Application.getDataFieldsMaxLength())
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.generateSignature = function (password, dataFields, successCallback, errorCallback) {
    argscheck.checkArgs('sAFF', 'Digipass.generateSignature', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'generateSignature', [password, dataFields]);
};

/**
 * Compute Client Server Time Shift From Server Time
 *
 * @param {Long} serverTimeInSeconds Server time in seconds
 * @param {Function} successCallback The function to call when the heading data is available
 * @param {Function} errorCallback The function to call when there is an error getting the heading data. (OPTIONAL)
 */
Digipass.prototype.computeClientServerTimeShiftFromServerTime = function (serverTimeInSeconds, successCallback, errorCallback) {
    argscheck.checkArgs('nFF', 'Digipass.computeClientServerTimeShiftFromServerTime', arguments);
    exec(successCallback, errorCallback, 'Digipass', 'computeClientServerTimeShiftFromServerTime', [serverTimeInSeconds]);
};

module.exports = new Digipass();
